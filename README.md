# Simple smtp-client (golang)

### Description
Simple smtp client on golang. Smtp client part based on [andelf gist](https://gist.github.com/andelf/5004821).
### Task 
Run infinite loop and send email (every X seconds).

Console arguments:
1. address receiptor of email
2. address sender of email
3. password (plain auth)
4. number of seconds for pause in infinte email sending process
5. message body

### Usage

```
go run smtp-client.go receiptor@example.com sender@example.com pass 5 'My message body'
```
### Task (RU)
Написать smtp-клиент, который

1) В качестве входных данных (аргументы командной строки) получает:

    адрес получателя, адрес отправителя, пароль.

2) Использует один из открытых smtp-серверов для доставки MIME-сообщений.
   
3)  Доставка сообщений выполняется с регулярным интервалом. Интервал и тело сообщения, имя файла для прикрепления (опционально) вводятся с клавиатуры. 