// Based on https://gist.github.com/andelf/5004821

package main

import (
	"encoding/base64"
	"fmt"
	"log"
	"net/mail"
	"net/smtp"
	"os"
	"strconv"
	"time"
)

// Command-line args: addressTo, addressFrom, password, timeInterval(seconds), messageBody
func main() {
	if len(os.Args) < 6 {
		log.Fatal("Must be arguments: addressTo, addressFrom, password, timeInterval(seconds), messageBody")
		os.Exit(1)
	}

	args := os.Args[1:]
	addressTo := args[0]
	addressFrom := args[1]
	password := args[2]
	timeIntervalSeconds, err := strconv.Atoi(args[3])
	checkError(err)
	messageBody := args[4]

	uptimeTicker := time.NewTicker(time.Duration(timeIntervalSeconds) * time.Second)

	smtpServerHost := "tserver" // host of local smtp server

	auth := smtp.PlainAuth(
		"",
		"sender@example.com",
		password,
		smtpServerHost,
	)

	from := mail.Address{Name: "Sender", Address: addressFrom}
	to := mail.Address{Name: "Receiver", Address: addressTo}

	header := make(map[string]string)
	header["From"] = from.String()
	header["To"] = to.String()
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = "text/plain; charset=\"utf-8\""
	header["Content-Transfer-Encoding"] = "base64"

	// Papercut SMTP server doesn't support AUTH, so we just print auth obj
	// and give nil instead of auth on smtp.SendMail(addr string, a smtp.Auth, from string, to []string, msg []byte)
	fmt.Println("SMTP auth:", auth)

	for range uptimeTicker.C {
		header["Subject"] = "Subject, sendTime: " + time.Now().Format("2006-01-02 15:04:05")
		message := ""
		for k, v := range header {
			message += fmt.Sprintf("%s: %s\r\n", k, v)
		}
		message += "\r\n" + base64.StdEncoding.EncodeToString([]byte(messageBody))

		err := smtp.SendMail(
			smtpServerHost+":25",
			nil, // auth,
			from.Address,
			[]string{to.Address},
			[]byte(message),
		)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Sent email")
	}

}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
